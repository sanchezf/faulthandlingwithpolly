using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RequestService.Api.Controllers
{
    [Route(RootRoute)]
    [ApiController]
    public class RequestController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private const string RootRoute = "api/[Controller]";
        
        public RequestController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetResponseAsync(int id)
        {
            var url = $"http://localhost:5109/api/Response/{id}";
            var httpClient = _httpClientFactory.CreateClient("Test");
            var response = await httpClient.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("---> Response returned failure");
                return StatusCode((int) HttpStatusCode.InternalServerError);
            }
            
            Console.WriteLine("---> Response returned SUCCESS");
            return Ok();
        }
        
    }
}