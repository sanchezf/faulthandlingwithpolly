using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace ResponseService.Api.Controllers
{
    [Route(RootRoute)]
    [ApiController]
    public class ResponseController : ControllerBase
    {
        private const string RootRoute = "api/[Controller]";

        [HttpGet("{id}")]
        public IActionResult GetResponse(int id)
        {
            Console.WriteLine($"--> Incoming request {DateTime.Now:HH:mm:ss.ffff} - id: {id}");
            var random = new Random();
            var randomInt = random.Next(1, 101);
            if (randomInt >= id)
            {
                Console.WriteLine($"--> Failure. Random value '{randomInt}' - Generate 500");
                return StatusCode((int) HttpStatusCode.InternalServerError);
            }
            return Ok();
        }
        
        
    }
}